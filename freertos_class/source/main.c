/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This is template for main module created by New Kinetis SDK 2.x Project Wizard. Enjoy!
 **/

#include <string.h>

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_debug_console.h"

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"


/* Task priorities. */
#define hello_task_PRIORITY (configMAX_PRIORITIES - 1)
#define task_n_PRIORITY (configMAX_PRIORITIES - 2)



/*!
 * @brief Task responsible for printing of "Hello world." message.
 */
static void hello_task(void *pvParameters) {
  uint32_t secs_counter = 0;
  for (;;) {
    vTaskDelay((const TickType_t)(1000));
    secs_counter++;
    PRINTF("Running for %i seconds.\r\n", secs_counter);
  }
}

static void rr_task1(void *pvParameters)
{
  uint32_t delay = 0x10000;
  for (;;)
  {
    /*
     * Modify the message to print the task name and task state for all the
     * round robin tasks
     * */
    PRINTF("I am task 1 for a round robin scheduling\n");
    while (--delay);
    delay = 0x10000;
  }
}

static void rr_task2(void *pvParameters)
{
  uint32_t delay = 0x10000;
  for (;;)
  {
    /*
     * Modify the message to print the task name and task state for all the
     * round robin tasks
     * */
    PRINTF("I am task 2 for a round robin scheduling\n");
    while (--delay);
    delay = 0x10000;
  }
}

static void rr_task3(void *pvParameters)
{
  uint32_t delay = 0x10000;
  for (;;)
  {
    /*
     * Modify the message to print the task name and task state for all the
     * round robin tasks
     * */

    PRINTF("I am task 3 for a round robin scheduling. NameTask: %s\n",pcTaskGetName(NULL));
    while (--delay);
    delay = 0x10000;
  }
}

/*!
 * @brief Application entry point.
 */
int main(void) {

  TaskHandle_t task_1_handler;
  TaskHandle_t task_2_handler;
  TaskHandle_t task_3_handler;

  /* Init board hardware. */
  BOARD_InitPins();
  BOARD_BootClockRUN();
  BOARD_InitDebugConsole();

  /* Add your code here */

  /* Create RTOS task */
  xTaskCreate(hello_task, "Hello_task", configMINIMAL_STACK_SIZE, NULL,
      hello_task_PRIORITY, NULL);

  xTaskCreate(rr_task1,"Task_1", configMINIMAL_STACK_SIZE, &task_1_handler, task_n_PRIORITY, &task_1_handler);
  xTaskCreate(rr_task2,"Task_2", configMINIMAL_STACK_SIZE, &task_2_handler, task_n_PRIORITY, &task_2_handler);
  xTaskCreate(rr_task3,"Task_3", configMINIMAL_STACK_SIZE, &task_3_handler, task_n_PRIORITY, &task_3_handler);

  /*
   * Create 3 tasks that operate as a round robin with lower priority than
   * hello task*/

  vTaskStartScheduler();

  for(;;) { /* Infinite loop to avoid leaving the main function */
    __asm("NOP"); /* something to use as a breakpoint stop while looping */
  }
}



