/*
 * Copyright (c) 2013 - 2016, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * This is template for main module created by New Kinetis SDK 2.x Project Wizard. Enjoy!
 **/

#include <string.h>

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_debug_console.h"

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "event_groups.h"

/* Task priorities. */
#define COM_PRIORITY	     (configMAX_PRIORITIES - 2)
#define CONTROL_PRIORITY		 (configMAX_PRIORITIES - 1)

#define COM_EVENT 		(1<<0)
#define CONTROL_EVENT 	(1<<1)
#define ALL_SYNC_BITS   (COM_EVENT | CONTROL_EVENT)

#define COM_EX_TIME     (10)
#define CONTROL_EX_TIME (40)
EventGroupHandle_t xEvent;

void com_task(void * parameters)
{
	TickType_t tick_base = xTaskGetTickCount();
	for (;;)
	{
		PRINTF("COM starting process\n");
		while (COM_EX_TIME > xTaskGetTickCount() - tick_base);
		PRINTF("COM finished process, waiting for rendezvous\n");

		xEventGroupSync(xEvent, COM_EVENT, ALL_SYNC_BITS, portMAX_DELAY);

		PRINTF("COM reached rendezvous\n");
		vTaskSuspend(*((TaskHandle_t*) parameters));
	}
}

void control_task(void * parameters)
{
	TickType_t tick_base = xTaskGetTickCount();
	for (;;)
	{
		PRINTF("CONTROL starting process\n");
		while (COM_EX_TIME > xTaskGetTickCount() - tick_base);
		PRINTF("CONTROL finished process, waiting for rendezvous\n");

		xEventGroupSync(xEvent, CONTROL_EVENT, ALL_SYNC_BITS, portMAX_DELAY);

		PRINTF("CONTROL reached rendezvous\n");
		vTaskSuspend(*((TaskHandle_t*)parameters));
	}
}

int main(void)
{

	TaskHandle_t Com_handle;
	TaskHandle_t Control_handle;
	/* Init board hardware. */
	BOARD_InitPins();
	BOARD_BootClockRUN();
	BOARD_InitDebugConsole();

	xEvent = xEventGroupCreate();

	/* Add your code here */

	/* Create RTOS task */
	xTaskCreate(control_task, "Control_task", configMINIMAL_STACK_SIZE, &Control_handle, CONTROL_PRIORITY, &Control_handle);
	xTaskCreate(com_task, "Communications_task", configMINIMAL_STACK_SIZE, &Com_handle, COM_PRIORITY, &Com_handle);
	vTaskStartScheduler();

	for (;;)
	{ /* Infinite loop to avoid leaving the main function */
		__asm("NOP");
		/* something to use as a breakpoint stop while looping */
	}
}

