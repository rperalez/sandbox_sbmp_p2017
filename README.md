# Sistemas Basados en MicroProcesadores Primavera 2017 #

### What is this repository for? ###

* This repository will be used to upload code examples and test Git and modify the examples that will be used during the Microprocessors Based Systems - Spring 2017 class at ITESO University. 
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You can install git from https://git-scm.com/downloads
* There are multiple git clients available. You can get a list [here](https://git-scm.com/downloads/guis)
* For software development we will work with [Kinetis Design Studio](http://www.nxp.com/products/software-and-tools/hardware-development-tools/freedom-development-boards/kinetis-design-studio-integrated-development-environment-ide:KDS_IDE)
* We will also need to build a Software Development Kit configuration for our K64. Visit https://mcuxpresso.nxp.com/en/ to get started with this
* Once Kinetis Design Studio and Kinetis SDK are in place, we will be able to use FreeRTOS to develop our projects. 

### Contribution guidelines ###

* Repository is restricted to the students attending this class. This is a read/write repository, be careful with what you do!
* Create a branch for any change that you make to a code in main
* To get familiar with the usage of this repository, please refer to the [Pro Git Book](https://git-scm.com/book/es/v1)

### Who do I talk to? ###

* For any comments or questions, please contact rperalez@iteso.mx